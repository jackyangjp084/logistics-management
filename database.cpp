#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <filesystem>

namespace fs = std::filesystem;

// 货物类
class Goods {
public:
    std::string name;
    std::string shippingCompany;
    std::string recipient;
    std::string sender;
    std::string packageSize;
    double shippingFee{};
    std::string deliveryPerson;
    std::string deliveryTime;
    int sequenceNumber{};
    bool inStock{};
};

// 检测文件是否存在，如果不存在则创建文件
void checkFileExistence(const std::string& filename) {
    if (!fs::exists(filename)) {
        std::ofstream outputFile(filename);
        if (!outputFile) {
            std::cout << "无法创建文件。\n";
            return;
        }
        outputFile.close();
    }
}

// 保存货物数据到 "data.txt" 文件
void saveGoodsData(const std::vector<Goods>& goodsList) {
    checkFileExistence("data.txt");

    std::ofstream outputFile("data.txt", std::ios::out | std::ios::binary | std::ios::trunc);
    outputFile << "\xEF\xBB\xBF"; // 添加BOM头，表示UTF-8编码
    if (!outputFile) {
        std::cout << "无法打开文件。\n";
        return;
    }

    for (const auto& goods : goodsList) {
        outputFile << goods.name << "," << goods.shippingCompany << "," << goods.recipient << ","
                   << goods.sender << "," << goods.packageSize << "," << goods.shippingFee << ","
                   << goods.deliveryPerson << "," << goods.deliveryTime << "," << goods.sequenceNumber << ","
                   << (goods.inStock ? "1" : "0") << "\n";
    }

    outputFile.close();
    std::cout << "数据已保存到 data.txt 文件。\n";
}

// 导出数据到 "data.csv" 文件
void exportToCsv(const std::vector<Goods>& goodsList) {
    checkFileExistence("data.csv");

    std::ofstream outputFile("data.csv", std::ios::out | std::ios::binary | std::ios::trunc);
    outputFile << "\xEF\xBB\xBF"; // 添加BOM头，表示UTF-8编码
    if (!outputFile) {
        std::cout << "无法打开文件。\n";
        return;
    }

    outputFile << "货物名称,物流公司,收件人,发件人,包裹尺寸,运费,送货人,送货时间,序列号,库存\n";
    for (const auto& goods : goodsList) {
        outputFile << goods.name << "," << goods.shippingCompany << "," << goods.recipient << ","
                   << goods.sender << "," << goods.packageSize << "," << goods.shippingFee << ","
                   << goods.deliveryPerson << "," << goods.deliveryTime << "," << goods.sequenceNumber << ","
                   << (goods.inStock ? "是" : "否") << "\n";
    }

    outputFile.close();
    std::cout << "数据已导出到 data.csv 文件。\n";
}
int main1() {
    checkFileExistence("data.txt");
    std::locale::global(std::locale(std::locale(), new std::codecvt_utf8_utf16<char16_t>));

    std::vector<Goods> goodsList;

    // 从 "data.txt" 文件导入数据
    std::ifstream inputFile("data.txt");
    if (!inputFile) {
        std::cout << "无法打开文件。\n";
        return 0;
    }

    std::string line;
    while (std::getline(inputFile, line)) {
        std::istringstream iss(line);
        std::string token;
        Goods goods;

        std::getline(iss, token, ',');
        goods.name = token;

        std::getline(iss, token, ',');
        goods.shippingCompany = token;

        std::getline(iss, token, ',');
        goods.recipient = token;

        std::getline(iss, token, ',');
        goods.sender = token;

        std::getline(iss, token, ',');
        goods.packageSize = token;

        std::getline(iss, token, ',');
        try {
            goods.shippingFee = std::stod(token);
        } catch (const std::invalid_argument& e) {
            std::cout << "无效的运费值: " << token << std::endl;
            // 处理错误的情况，例如给shippingFee赋予默认值或者提示用户重新输入
        }
        std::getline(iss, token, ',');
        goods.deliveryPerson = token;

        std::getline(iss, token, ',');
        goods.deliveryTime = token;

        std::getline(iss, token, ',');
        try {
            goods.sequenceNumber = std::stoi(token);
        } catch (const std::invalid_argument& e) {
            std::cout << "无效的序列号值: " << token << std::endl;
            // 处理错误的情况，例如给sequenceNumber赋予默认值或者提示用户重新输入
        }

        std::getline(iss, token);
        goods.inStock = (token == "1");

        goodsList.push_back(goods);
    }

    inputFile.close();

    // 保存数据到文件
    saveGoodsData(goodsList);

    // 导出CSV文件
    exportToCsv(goodsList);

    return 0;
}
//
// Created by asus on 2023/7/5.
// Develop by sheng & kserks
//
